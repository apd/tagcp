#!/usr/bin/python

# Simple script to copy MP4 artist, title, 
# and track information from an xTunes AAC file
# to an MP3

# Requires Mutagen. 
#	http://http://code.google.com/p/mutagen/

from mutagen.mp4 import MP4,MP4StreamInfoError
from mutagen.mp3 import MP3,HeaderNotFoundError
from mutagen.id3 import ID3,TIT2,TALB,TPE1,TRCK

def doc():
	print """
	Not enough arguments:
	
	Usage: tagcp.py <source mp4> <dest mp3>
	
	"""

def copy_tag(old,new):
	try:
		source,dest = MP4(old), MP3(new)
		dest["TIT2"] = TIT2(encoding=3, text=source["\xa9nam"]) # title
		dest["TALB"] = TALB(encoding=3, text=source["\xa9alb"]) # album
		dest["TPE1"] = TPE1(encoding=3, text=source["\xa9ART"]) # artist

		print "Title = " + str(dest["TIT2"])
		print "Album = " + str(dest["TALB"])
		print "Artist = " + str(dest["TPE1"])


		# Track is a little more complicated
		trkn = source["trkn"].pop()
		tnum = trkn[1]
		print "tnum = " + str(tnum)
		dest["TRCK"] = TRCK(encoding=3, text=str(tnum)) # track number

		print "Track Number = " + str(dest["TRCK"])
		dest.save()

		print "Great Success!"
		
	except MP4StreamInfoError: 
		print "Source file is not an mp4!"
	except HeaderNotFoundError:
		print "Destination file is not an mp3!"
	
if __name__ == "__main__": 
	import sys
	try:
		copy_tag(sys.argv[1],sys.argv[2])
	except IndexError:
		doc()